/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

/**
 *
 * @author Ricardo
 */
public class calculando {

   
    private int capital;
    private float tasa;
    private int anios;
    private int interes;
    
    
     /**
     * @return the interes
     */
    public int getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(int interes) {
        this.interes = interes;
    }

    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the tasa
     */
    public float getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(float tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the anios
     */
    public int getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(int anios) {
        this.anios = anios;
    }

    public int calcularinteres(int capital, float tasa, int anios) {
        this.setCapital(capital);
        this.setTasa(tasa);
        this.setAnios(anios);

        return interes;

    }
    
    public float calcularinteres() {
        
       return this.getCapital()*this.getTasa()*this.getAnios();

    }
;

}
